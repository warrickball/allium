import numpy as np
import unittest
import allium

class TestAlliumSphere(unittest.TestCase):

    def test_radial_modes_isothermal_sphere(self):
        for k in [2**i for i in range(7)]:
            s = allium.Sphere(c=np.ones(k), R=np.linspace(0, 1, k+1)[1:])
            s.search_eigenfrequencies(0, np.arange(0.5, 25.0, 1.)*np.pi)
            np.testing.assert_allclose(s.eigenfrequencies[0], np.arange(1.0, 25.0)*np.pi)

            r = np.linspace(1e-4, 1, 101)
            eig = s.eigenfunction(0, 3)
            np.testing.assert_allclose(eig(r), np.sin(4*np.pi*r)/(4*np.pi*r), atol=1e-14)


    def test_splitting_layers(self):
        l = [0,1,5,15]
        f = np.arange(1e-4, 20., 0.2)*np.pi
        r = np.linspace(0, 1, 51)
        
        ref = allium.Sphere(c=[1.2, 1.1, 0.9, 1.0],
                           R=[0.1, 0.2, 0.4, 1.])
        ref.search_eigenfrequencies(l, f)

        for i in range(len(ref.c)):
            c = ref.c.copy()
            R = ref.R.copy()
            c.insert(i, ref.c[i])
            R.insert(i, ref.R[i]-ref.dR[i]/2)
            split = allium.Sphere(c=c, R=R)
            np.testing.assert_allclose(split.Delta_omega, ref.Delta_omega)

            split.search_eigenfrequencies(l, f)
            for li in l:
                np.testing.assert_allclose(split.eigenfrequencies[li], ref.eigenfrequencies[li])

                np.testing.assert_allclose(split.eigenfunction(li, -1)(r),
                                           ref.eigenfunction(li, -1)(r), atol=1e-14)


    def test_asymptotic_large_separation(self):
        s = allium.Sphere(c=[1.15, 0.95, 0.9, 1.1, 1.05, 1.0],
                         R=[0.1, 0.2, 0.3, 0.4, 0.5, 1.0])
        s.search_eigenfrequencies(0, np.arange(60.5, 70., 0.25)*np.pi)
        linear = np.polyfit(np.arange(len(s[0])), s[0,:], 1)[0]
        np.testing.assert_allclose(linear, s.Delta_omega, rtol=1e-3)


    def test_attribute_lengths(self):
        s = allium.Sphere(c=[1.1, 1.0, 0.9], R=[1.0])

        with self.assertRaises(AssertionError):
            s.search_eigenfrequencies(0, [1.7, 5.1])

        with self.assertRaises(AssertionError):
            s.wavefunction(0, np.pi)
        
        s.R = np.linspace(0, 1, 6)[1:]

        with self.assertRaises(AssertionError):
            s.search_eigenfrequencies(0, [1.7, 5.1])

        with self.assertRaises(AssertionError):
            s.wavefunction(0, np.pi)
