# allium

Compute oscillations of radially symmetric things.

**Install**: `pip install allium <--user>`

**Docs**: https://warrickball.gitlab.io/allium
