.. allium documentation master file, created by
   sphinx-quickstart on Wed Apr 14 21:42:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

``allium``
==========

``allium`` computes eigenfunctions and eigenfrequencies for
radially-symmetric things (currently only a sphere) assuming the
sound speed is a piecewise constant function.  That is, it solves
the radial part of the `Helmholtz equation`_

.. math::

   \nabla^2 f + \frac{\omega^2}{c^2} f = 0

assuming that the sound speed :math:`c` is of the form

.. math::

   c(r) = \left\{ \begin{array}{lcr}
                     c_1 & \mathrm{if} &       0 < r < R_1 \\
                     c_2 & \mathrm{if} &     R_1 < r < R_2 \\
		     \ldots \\
                     c_n & \mathrm{if} & R_{n-1} < r < R_n \\
		  \end{array} \right.

with boundary conditions :math:`f(R_n)=0` and regularity at :math:`r=0`.

You can install ``allium`` from PyPI using e.g.

Installation
------------

::

    python -m pip install allium <--user>

Basic usage
-----------

The objects (e.g. :py:class:`.Sphere`) can compute eigenfrequencies.
e.g. for a sphere with :math:`\vec{c}=(c_1,c_2)=(1.05, 1.0)` and
:math:`\vec{R}=(R_1, R_2)=(0.1, 1.0)`, we can create an echelle diagram:

.. plot::
   :include-source:

   import numpy as np
   import matplotlib.pyplot as pl
   from allium import Sphere

   s = Sphere(c=[1.05, 1.0], R=[0.1, 1.0])
   s.search_eigenfrequencies([0,1,2,3], np.arange(0.5, 20.0)*np.pi)
   for l in range(4):
       w = np.array(s[l])/s.Delta_omega
       pl.plot(np.mod(w-0.01, 1.0)+0.01, w, 'o', label='ℓ=%i' % l)

   pl.xlabel('ω/Δω mod 1')
   pl.ylabel('ω/Δω')
   pl.xlim(-0.05, 1.05)
   pl.legend()

We can also compute wavefunctions, which only satisfy the inner boundary
condition, or eigenfunctions, which satisfy both.  For a more extreme
sphere with :math:`\vec{c}=(c_1,c_2)=(2.0, 0.5)` and
:math:`\vec{R}=(R_1,R_2)=(0.5, 1.0)`, here's an eigenfunction and a
not-eigenfunction:

.. plot::
   :include-source:

   import numpy as np
   import matplotlib.pyplot as pl
   from allium import Sphere

   s = Sphere(c=[2.0, 0.5], R=[0.5, 1.0])
   s.search_eigenfrequencies(2, np.arange(10.5, 15.0, 0.5)*np.pi)

   r = np.linspace(0, 1, 1001)
   pl.plot(r, s.eigenfunction(2, 0)(r), label='eigenfunction')
   pl.plot(r, s.wavefunction(2, s[2,0]+0.5*s.Delta_omega)(r),
           label='not an eigenfunction')
   pl.legend()
   pl.xlabel('r')
   pl.ylabel('f')
		 
You can read about `how <how.html>`_ ``allium`` works or `why <why.html>`_
you might use it and I created it, or go to the APIs to learn how to
use each object.

.. _Helmholtz equation: https://en.wikipedia.org/wiki/Helmholtz_equation

.. toctree::
   :maxdepth: 2
   :caption: About

   how
   why

.. toctree::
   :maxdepth: 2
   :caption: APIs

   sphere

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
