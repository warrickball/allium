.. |ℓ| replace:: :math:`\ell`

Why?
====

Why should I use ``allium``?
----------------------------

``allium``'s main purpose is to solve a highly simplified version of the
equations for stellar oscillations.  It's very limited (e.g. there are
no gravity modes) but can hopefully provide some useful insights and
perhaps serve as a basis on which to develop simplified versions of
other, related, problems (e.g. inversion).

Why did you write ``allium``?
-----------------------------

I study `solar-like oscillations`_: high-order, low-degree
oscillations in stars that are qualitatively similar to the Sun.
The mode frequencies in these stars obey an approximate
asymptotic relation (in the limit :math:`n\gg\ell`)

.. math::

   \nu_{\ell,n}=\Delta\nu\left(n+\frac{1}{2}\ell+\epsilon\right)-D\ell(\ell+1)

where :math:`n` is the mode's *radial order*, |ℓ| its *angular degree*
and the other symbols are various parameters that are roughly constant
but whose meaning isn't important at this level.  The relation implies
that, for a given |ℓ|, the modes frequencies modulo a *large
separation* :math:`\Delta\nu` are roughly equal.  So if we plot
something that increases with frequency (e.g. :math:`\nu_{\ell,n}` or
:math:`\nu_{\ell,n}/\Delta\nu`) against
:math:`\nu_{\ell,n}\,\mathrm{mod}\,\Delta\nu`, the modes of each |ℓ|
should form vertical ridges.
   
For example, the low-degree mode frequencies of the Sun, measured by
`BiSON <http://bison.ph.bham.ac.uk/portal/frequencies>`_ and using
:math:`\Delta\nu=135.1\,\mu\mathrm{Hz}`, produce this echelle diagram:

.. plot::

   import numpy as np
   import matplotlib.pyplot as pl

   Dnu = 135.1
   
   DS = np.DataSource(None)
   dtype = [('n',int),('l',int),('freq',float),('err',float)]

   # < 2mHz
   lo = np.loadtxt(DS.open('http://bison.ph.bham.ac.uk/downloads/data/davies2014.txt'),
                   dtype=dtype)
   # > 2mHz
   hi = np.loadtxt(DS.open('http://bison.ph.bham.ac.uk/downloads/data/broomhall2009.txt'),
                   dtype=dtype)
   hi = hi[hi['freq']>2e3]
		   
   for li in range(4):
       I = lo['l'] == li
       pl.plot(lo['freq'][I]%Dnu, lo['freq'][I]/Dnu, 'C%io' % li, label='ℓ=%i' % li)

       I = hi['l'] == li
       pl.plot(hi['freq'][I]%Dnu, hi['freq'][I]/Dnu, 'C%io' % li)

   pl.xlim(0, Dnu)
   pl.xlabel('ν mod Δν')
   pl.ylabel('ν/Δν')
   pl.legend()
   
I was working on a project for which I needed to generate artificial
data.  The data didn't need to be particularly accurate and I was
using the asymptotic relation above, which, using solar values, gives:

.. plot::

   import numpy as np
   import matplotlib.pyplot as pl

   n = np.arange(6,30)
   eps = 1.45
   Dnu = 135.1
   D = 1.5

   for l in range(4):
       nu = (n+l/2+eps)*Dnu - D*l*(l+1)
       pl.plot(nu%Dnu, nu/Dnu, 'o', label='ℓ=%i' % l)

   pl.xlabel('ν mod Δν')
   pl.ylabel('ν/Δν')
   pl.xlim(0, Dnu)
   pl.legend()

I wondered if I might be a bit better off shifting and scaling the
oscillations of an isothermal (i.e. constant sound speed) sphere,
whose echelle diagram looks like this:

.. plot::

   import numpy as np
   import matplotlib.pyplot as pl
   from allium import Sphere

   s = Sphere()
   s.search_eigenfrequencies([0,1,2,3], np.arange(0.5, 20.0)*np.pi)
   for l in range(4):
       w = np.array(s[l])/s.Delta_omega
       pl.plot(np.mod(w-0.01, 1.0)+0.01, w, 'o', label='ℓ=%i' % l)

   pl.xlabel('ω/Δω mod 1')
   pl.ylabel('ω/Δω')
   pl.xlim(-0.05, 1.05)
   pl.legend()

This made me wonder: what about using a sphere with *two* layers at
different (though constant) sound speeds?  I presumed that this would
be textbook work and searched for a solution but to no avail.  So I
managed to work it out and subsequently how to extend it to an
arbitrary number of layers.

.. plot::

   import numpy as np
   import matplotlib.pyplot as pl
   from allium import Sphere

   s = Sphere(c=[1.09, 1.03, 1.0],
              R=[0.05, 0.12, 1.0])
   s.search_eigenfrequencies([0,1,2,3], np.arange(0.5, 20.0)*np.pi)
   for l in range(4):
       w = np.array(s[l])/s.Delta_omega
       pl.plot(np.mod(w-0.01, 1.0)+0.01, w, 'o', label='ℓ=%i' % l)

   pl.xlabel('ω/Δω mod 1')
   pl.ylabel('ω/Δω')
   pl.xlim(-0.05, 1.05)
   pl.legend()

I've realised that the method should extend quite easily to two
dimensions (vibrations on a disc or drum) and one (vibrations on a
string with particular boundary conditions).  But I'll implement that
later™...

.. _solar-like oscillations: https://en.wikipedia.org/wiki/Solar-like_oscillations
